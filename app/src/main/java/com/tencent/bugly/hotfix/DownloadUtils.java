package com.tencent.bugly.hotfix;

import android.os.Environment;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Shen Chao.
 * Created on 2018-10-23.
 */
public class DownloadUtils {
    private static String fileName = "patch_signed_7zip.apk";

    @Deprecated
    public static void downFix() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    URL url = new URL("http://172.19.181.53:8080/patch_signed_7zip.apk");
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.connect();
                    int code = conn.getResponseCode();
                    if (code == 200) {
                        String path = Environment.getExternalStorageDirectory().getPath() + "/1/" + fileName;
                        InputStream is = conn.getInputStream();
                        File file = new File(path);
                        FileOutputStream fos = new FileOutputStream(file);
                        byte[] bytes = new byte[1024];
                        int len;
                        while ((len = is.read(bytes)) != -1) {
                            fos.write(bytes, 0, len);
                        }
                        fos.flush();
                        is.close();
                        fos.close();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();

    }
}
